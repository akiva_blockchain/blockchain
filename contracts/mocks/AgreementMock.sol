pragma solidity 0.5.11;

import '../Agreement.sol';
import './ConfigMock.sol';

/*
 * @title Base Agreement Mock contract
 * @dev Should not be deployed. It is being used as an abstract class
 */
contract AgreementMock is Agreement {
    uint public dsrTest = 105 * 10 ** 25;
    uint256 public currentTime;
    uint256 public unlockedDai;
    address erc20Token;
    address public mcdDaiAddrMock;

    /**
     * @notice should be removed after testing!!!
     */
    function setDelta(int _delta) public {
        delta = _delta;
    }

    function setDsr(uint _dsrTest) public {
        dsrTest = _dsrTest;
    }

    function getDsr() public view returns(uint) {
        return dsrTest;
    }

    function setCurrentTime(uint256 _time) public {
      currentTime = _time;
    }

    function getCurrentTime() public view returns(uint256) {
      return currentTime;
    }

    function _openCdp(bytes32 ilk) internal returns (uint cdp) {
        return 0;
    }

    function _lockDai(uint wad) internal {}

    function _lockETHAndDraw(bytes32 ilk, uint cdp, uint wadC, uint wadD) internal {}

    function _lockERC20AndDraw(bytes32 ilk, uint cdp, uint wadD, uint wadC, bool transferFrom) internal {}

    function _transferDai(address to, uint amount) internal returns(bool) {
      return true;
    }

    function setMcdDaiAddrMock(address _addr) public {
      mcdDaiAddrMock = _addr;
    }

    function _transferFromDai(address from, address to, uint amount) internal returns(bool) {
      ERC20Interface(mcdDaiAddrMock).transferFrom(from, to, amount);
      return true;
    }
    
    function setUnlockedDai(uint256 _amount) public {
      unlockedDai = _amount;
    }

    function _unlockDai(uint256 _amount) internal {}

    function _unlockAllDai() internal returns(uint pie) {
      return unlockedDai;
    }

    function _injectToCdp(uint cdp, uint wad) internal {}

    function _forceLiquidateCdp(bytes32 ilk, uint cdpId) internal view returns(uint) {
      return 0;
    }

    function getCollateralEquivalent(bytes32 ilk, uint daiAmount) public view returns(uint) {
      return daiAmount * 200;
    }

    function _initMcdWrapper() internal {}

    function setErc20Token(address _contract) public {
      erc20Token = _contract;
    }

    function erc20TokenContract(bytes32 ilk) public view returns(ERC20Interface) {
      return ERC20Interface(erc20Token);
    }

    function setStatus(uint256 _status) public {
      status = _status;
    }

    function initAgreement(
      address payable _borrower,
      uint256 _collateralAmount,
      uint256 _debtValue,
      uint256 _duration,
      uint256 _interestRatePercent,
      bytes32 _collateralType,
      bool _isETH,
      address configAddr
    ) public payable {
      super.initAgreement(_borrower, _collateralAmount, _debtValue, 
        _duration, _interestRatePercent, _collateralType, _isETH, configAddr);
      
      setErc20Token(ConfigMock(configAddr).getErc20collToken());
    }

    function updateAgreementState() public returns(bool success) {
      return _updateAgreementState();
    }

    function setLastCheckTime(uint256 _value) public {
      lastCheckTime = _value;
    }
}


contract AgreementDeepMock is AgreementMock {
  function _transferFromDai(address from, address to, uint amount) internal returns(bool) {
      return true;
    }
}